<?php
// DIC configuration
$container = $app->getContainer();

$container['view'] = new \Slim\Views\PhpRenderer(__DIR__ . '/Templates/');

// Setup PDO DB connection
// $container['db'] = function ($c) {
//     $db = $c['settings']['database'];
//     $pdo = new PDO('mysql:host=' . $db['host'] . ';dbname=' . $db['dbname'] . ';charset=utf8',
//         $db['username'], $db['password']);
//     $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//     $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
//     return $pdo;
// };

// Handle all non-framework errors
$container['errorHandler'] = function ($c) {
    return function ($request, $response, $exception) use ($c) {

        $env =  $c->get('settings')['env'];

        // //Format of exception to return
        // $db =       $c['db'];
        // if($c->has('user')) {
        //     $user =     $c['user'];
        // }else{
        //     $user =     ['user_id' => 0];
        // }
        

        // $stmt = $db->prepare('INSERT INTO error_log (`sam`,`user_id`,`error`,`file`,`line`) VALUES (0,?,?,?,?)');
        
        $error_data = [
                        $exception->getMessage(),
                        $exception->getFile(),
                        $exception->getLine()
                    ];

        // $stmt->execute($error_data);

        $data = [
            'message'       => $exception->getMessage(),
            'error_data'    => $error_data,
            'file'          => $exception->getFile(),
            'line'          => $exception->getLine()
        ];
        if($env != 'production'){
            return $response->withJson(['success' => false,'error' => 'An error occured', 'error_data' => $data]);
        }else{
            return $response->withJson(['success' => false,'error' => 'An error occured']);
        }
        
    };
};