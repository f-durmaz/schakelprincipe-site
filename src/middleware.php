<?php
// Application middleware
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

use Dflydev\FigCookies\FigRequestCookies;
use Dflydev\FigCookies\FigResponseCookies;
use Dflydev\FigCookies\SetCookie;

$app->add(function (Request $request, Response $response, callable $next) {
    $uri = $request->getUri();
    $path = $uri->getPath();
    if ($path != '/' && substr($path, -1) == '/') {
        // permanently redirect paths with a trailing slash
        // to their non-trailing counterpart
        $uri = $uri->withPath(substr($path, 0, -1));
        
        if($request->getMethod() == 'GET') {
            return $response->withRedirect((string)$uri, 301);
        }
        else {
            return $next($request->withUri($uri), $response);
        }
    }

    return $next($request, $response);
});

$authentication_mw = function ($request, $response, $next) {
    $settings = $this->get('settings');
    
    $cookie = FigRequestCookies::get($request, $settings['app']['cookie_name']);
    $token = filter_var($cookie->getValue(),FILTER_SANITIZE_STRING);
    
    if(!$token){
        return $response->withRedirect('/login');
    }

    $sql = 'SELECT a.created_on,a.renewed_on,(NOW() - INTERVAL 30 MINUTE) as db_now,a.comm_id,a.conn_id,
                b.user_id,b.orga_id,b.sam,b.active,b.role,b.about,b.campaign,
                c.firstname,c.lastname,c.street,c.postal,c.city,c.avatar
            FROM portal_cookie_token AS a
            LEFT JOIN user_orga AS b ON a.conn_id = b.conn_id
            LEFT JOIN users AS c ON b.user_id = c.user_id
            WHERE a.token = ? AND a.valid = 1';

    $stmt = $this->db->prepare($sql);
    $stmt->execute([$token]);
    if(!$user_data = $stmt->fetch()){
        return $response->withRedirect('/login');
    }

    # Renew cookie if older than 30 minutes and the user is still active, cookie expires in 1 hour
    if(strtotime($user_data['renewed_on']) < strtotime($user_data['db_now'])){
        // We last renewed our token more than 30 minutes ago renew our token again
        $response = FigResponseCookies::set($response, SetCookie::create($settings['app']['cookie_name'])
            ->withValue($token)
            ->withDomain($settings['app']['cookie_domain'])
            ->withPath($settings['app']['cookie_path'])
            ->withMaxAge(14400)
            ->withSecure($settings['app']['secure'])
            ->withHttpOnly($settings['app']['secure'])
        );

        $this->db->query('UPDATE portal_cookie_token SET renewed_on = NOW() WHERE token = "'.$token.'"');
    }else{

    }

    # No need to share this info with the world
    unset($user_data['created_on']);
    unset($user_data['renewed_on']);

    $community_data = [];
    // Get community data
    $sql = 'SELECT a.community_name, a.comm_token, a.logo, a.front_domain, a.slogan, a.logo_width, a.logo_height, a.address,
                    b.subject_type_id AS offer_type_id, b.type_name AS offer_type, c.subject_type_id AS request_type_id, c.type_name AS request_type,
                    d.primary_color,d.secondary_color,d.button_color,d.menu_databank,d.menu_databank_name,d.menu_network,d.menu_network_name,d.menu_participants,d.menu_participants_name,d.menu_about,d.menu_about_name,d.menu_about_url,
                    cs.*
            FROM community AS a
            LEFT JOIN community_settings AS cs ON a.comm_id = cs.comm_id
            LEFT JOIN subject_type AS b ON a.offer_type = b.subject_type_id
            LEFT JOIN subject_type AS c ON a.request_type = c.subject_type_id
            LEFT JOIN portal_themes AS d ON a.theme = d.theme_id
            WHERE a.comm_id = ?';
    $stmt = $this->db->prepare($sql);
    $stmt->execute([
        $user_data['comm_id']
    ]);
    $community_data['settings'] = $stmt->fetch();

    // Get community discipline
    $sql = 'SELECT a.discipline_id, b.discipline_name
            FROM community_discipline AS a
            LEFT JOIN discipline AS b ON a.discipline_id = b.discipline_id
            WHERE a.comm_id = ?';
    $stmt = $this->db->prepare($sql);
    $stmt->execute([
        $user_data['comm_id']
    ]);
    $community_data['discipline'] = $stmt->fetchAll();

    // Load user data into container so it is accessible for all classes
    $this->user = $user_data;

    // Load community data into container
    $this->community = $community_data;

    return $next($request, $response);
};

$auth_room_admin = function ($request, $response, $next){ 
    if($this->user['role'] < 79){
        $response = $response->withStatus(401);
        $response = $response->withJson($result);
        return $response;
    }else{
        return $next($request, $response);
    }
};