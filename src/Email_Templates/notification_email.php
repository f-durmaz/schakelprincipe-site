<?php

return '<html><body style="background:#f8f8f8;"><table width="100%" border="0" cellpadding="0" cellspacing="0" style="background:#f8f8f8;">
<tr><td style="height:20px;"></td></tr>
    <tr>
        <td align="center">
            <table border="0" cellpadding="0" cellspacing="0" style="width:500px;background:white;border-collapse:collapse;border:1px solid rgb(220,220,220);font-size: 14px;font-family: Tahoma, Geneva, Verdana, sans-serif;">
                <tr><td><span style="color:white;">community_name notificatie.</span></td></tr>
                <tr style="width:100%">
                    <td style="width:100%"><table style="width:100%;border-collapse:collapse;"><tr style="width:100%">
                        <td width="500px;" style="width:500px;text-align:center;"><img src="https://schakelprincipe.nl/assets/images/logos/sis.png" width="portal_logo_width" height="portal_logo_height" style="width:portal_logo_widthpx;max-width:portal_logo_widthpx;" /></td>
                    </tr></table></td>
                </tr>
                <tr><td><span style="color:white;">community_name</span></td></tr>
                
                <tr style="background:rgb(community_color);">
                    <td align="center">
                        <table style="width:80%;border-collapse:collapse;font-family: Tahoma, Geneva, Verdana, sans-serif;">
                            <tr style="background:rgb(community_color);height:20px;width:100%;"><td><span style="color:rgb(community_color);">---</span></td></tr>
                            <tr><td style="font-size:20px;padding:0px;margin:0px;margin:0px;color:#FFFFFF">community_name: Notificatie</td></tr>
                            <tr style="background:rgb(community_color);height:20px;width:100%;"><td><span style="color:rgb(community_color);">---</span></td></tr></table>
                    </td>
                </tr>
                <tr style="height:40px;width:100%;"><td><span style="color:white;">---</span></td></tr>
                
                <tr><td align="center">
                    <table style="width:80%;border-collapse:collapse;font-size: 14px;font-family: Tahoma, Geneva, Verdana, sans-serif;"><tr><td>
                        <p>
                            Beste,
                        </p>
                        
                        not_message_here
                    </td></tr></table>
                    </td>
                </tr>
                
                <tr style="height:20px;width:100%;"><td><span style="color:white;">---</span></td></tr>

                <tr><td align="center">
                    <table style="width:80%;border-collapse:collapse;font-size: 14px;font-family: Tahoma, Geneva, Verdana, sans-serif;"><tr><td>
                        <p>
                            Met vriendelijke groeten,<br />
                            <br />
                            Team <span style="rgb(community_color)">community_name</span>
                        </p>
                    </td></tr></table>
                </td></tr>
                
                <tr style="height:20px;width:100%;"><td><span style="color:white;">---</span></td></tr>
                <tr style="background:rgb(community_color);height:60px;"><td align="center"><table style="width:90%;border-collapse:collapse;text-align:center;"><tr><td><span style="text-align:center;font-family: Tahoma, Geneva, Verdana, sans-serif;font-size:12px;color:#FFFFFF;">Dit is een automatisch bericht</span></td></tr></table></td></tr>
                <tr style="height:20px;width:100%;"><td><span style="color:white;">---</span></td></tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" style="width:500px;font-size: 12px;font-family: Tahoma, Geneva, Verdana, sans-serif;">
                <tr style="background:#f8f8f8;height:10px;width:100%;"><td><span style="color:#f8f8f8;">---</span></td></tr>
                <tr><td style="text-align:center">
                    <p>Je ontvangt deze email omdat je je beheerder bent voor community_name.</p>
                    <p>SchakelPrincipe - Stadionweg 70C, 6225 XR Maastricht</p>
                </td></tr>
            </table>
        </td>
    </tr>
    <tr style="background:#f8f8f8;height:60px;width:100%;"><td><span style="color:#f8f8f8;">---</span></td></tr>
</table></body></html>';