<?php
namespace SchakelPrincipe;

use Psr\Container\ContainerInterface;
use Mailgun\Mailgun;

class EmailController{
    private $app;
    private $mg;

    public function __construct(ContainerInterface $container) {
        $this->app = $container->get('settings')['app'];
        $this->mg = Mailgun::create($this->app['mailgun_key'], 'https://api.eu.mailgun.net');
    }

    /**
     * Send an email using the Mailgun SDK
     * 
     * @param Array user_data
     * @param Array email_data
     * @param String template
     * @param String plain template
     * 
     * @return bool
     */

    public function send_question($parsedData){
        $template = require dirname(__DIR__,1) . '/Email_Templates/notification_email.php';

        $name = filter_var($parsedData['name'], FILTER_SANITIZE_STRING);
        $email = filter_var($parsedData['email'], FILTER_SANITIZE_EMAIL);
        $tel = filter_var($parsedData['tel'], FILTER_SANITIZE_STRING);
        $question = filter_var($parsedData['question'], FILTER_SANITIZE_STRING);

        $message = '<p>Er is een vraag of opmerking via de website verstuurd.</p><p>';
        $message .= '<b>'.$name.'</b><br />';
        $message .= 'E: '.$email.' <br />';
        $message .= 'T: '.$tel.' <br />';
        $message .= '</p>';
        $message .= '<p><b>Vraag</b><br />'.$question.'</p>';
        $message .= '<p><i>Let op: Het formulier van waaruit deze notificatie is verstuurd, is toegankelijk voor het hele internet goed of kwaad. Klik daarom nooit op een link die je niet vertrouwd en reageer niet op emails die verdacht lijken</i></p>';

        $email_data = [];
        $email_data['to']               = 'SchakelPrincipe <info@schakelprincipe.nl>';
        // $email_data['to']               = 'Daan Rijnkels <daan.rijnkels@schakelbv.com>';
        $email_data['cc']               = 'Daan Rijnkels <daan.rijnkels@schakelbv.com>, Philippe Heijnen <philippe.heijnen@schakelbv.com>';
        $email_data['subject']          = 'Vraag of Opmerking';
        $email_data['v:email_type']     = 'website_contact';
        $email_data['o:testmode']       = false;
        $email_data['o:tracking']       = false;
        $email_data['o:tags']           = ['notification','website_contact'];
        // $email_data['recipient-variables'] = json_encode(['daan.rijnkels@schakelbv.com' => ['domain' => $email_settings['front_domain']]]);

        // $email_settings = $this->_get_email_settings(['comm_id' => 1]);

        // $email_data['from'] = $email_settings['community_name'].' - NetwerkSchakel <'.$this->app['mailgun_email'].'>';
        $email_data['from'] = 'SchakelPrincipe <info@schakelprincipe.nl>';

        # Edit the email template with community specific colors / names / logos / address
        $template = preg_replace('/community_color/',	        '230, 116, 55',		    $template);
        $template = preg_replace('/action_color/',	            '230, 116, 55',	        $template);
        $template = preg_replace('/basedomain/',	            'https://schakelprincipe.nl/',       $template);
        $template = preg_replace('/domain/',	                'https://schakelprincipe.nl/', $template);
        $template = preg_replace('/not_message_here/',	        nl2br($message),		                    $template);
        $template = preg_replace('/community_name/',	        'Schakel Principe',          $template);
        $template = preg_replace('/portal_address/',	        'Stadionweg 70C, 6225 XR Maastricht',		            $template);
        $template = preg_replace('/portal_logo_width/',	        145,	            $template);
        $template = preg_replace('/portal_logo_height/',	    80,             $template);
        // $template = preg_replace('/portal_logo/',	            $email_settings['logo'],		            $template); // Make sure this one is always after any other portal_logo settings
        $email_data['html'] = $template;

        // return ['send_email',$email_data,$this->app['mailgun_domain']];

        # Send our email to the Mailgun API
        $result = $this->mg->messages()->send('mg.netwerkschakel.com',$email_data);

        # Getting the actual API response code has been removed from the API but if we made it this far it means that Mailgun send back a 200
        $email_data['success'] = true;

        # Capture the API response
        $email_data['mailgun_id'] = $result->getId();
        $email_data['mailgun_message'] = ($result->getMessage()) ? $result->getMessage() : 'Could not get Mailgun API response';

        return $email_data['success'];
    }
}