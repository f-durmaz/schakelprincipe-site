<?php
require 'env.php';

# composer dumpautoload -o

switch($env){
    case 'local':
        $base_domain    = 'schakelprincipe.local';
        $cookie_domain  =    '';
        $cookie_folder  =    '/';
        $mailgun_email  =    'info@netwerkschakel.dev';
        $mailgun_domain =   'mg.netwerkschakel.dev';
        $secure =           false;
        $error_log =        __DIR__ . '/log/netwerkschakel.log';
        break;
    case 'test':
        
        break;
    case 'live':
    case 'production':
        $base_domain    = 'schakelprincipe.nl';
        $cookie_domain  =    'schakelprincipe.nl';
        $cookie_folder  =    '/';
        $mailgun_email  =    'info@netwerkschakel.com';
        $mailgun_domain =   'mg.netwerkschakel.com';
        $secure =           true;
        $error_log =        __DIR__ . '/log/schakelprincipe.log';
        break;
}

return [
    'settings' => [
        'displayErrorDetails' => true,
        'addContentLengthHeader' => false,

        'env'       =>  $env,

        // Monolog settings
        // Monolog lvls DEBUG,INFO,NOTICE,WARNING,ERROR,CRITICAL,ALERT,EMERGENCY
        'logger'        => [
            'name'  => 'app_logger',
            'path'  => $error_log
        ],

        'database'      =>  [
            'host'          =>  'localhost',
            'dbname'        =>  'netwerkschakel',
            'username'      =>  $db_user,
            'password'      =>  $db_password,
            'charset'       =>  'utf8'
        ],
        
        'app'       =>  [
            'base_domain'       => $base_domain,
            'cookie_name'       => 'netwerkschakel',
            'cookie_domain'     => $cookie_domain,
            'cookie_path'       => $cookie_folder,
            'secure'            => $secure,
            'mailgun_key'       => $mailgun_key,
            'mailgun_email'     => $mailgun_email,
            'mailgun_domain'    => $mailgun_domain,
            'file_folder'       => __DIR__ . '/user_files'
        ],
    ],
];