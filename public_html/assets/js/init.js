$(document).ready(function () {
    

    var sendQuestion = function(){
        $('#contact_form .formError').hide().html()
        $('#contact_form .contact__btn').hide();

        var name = $('#name').val();
        var email = $('#email').val();
        var tel = $('#tel').val();
        var question = $('#question').val();
        var privacy = ($('#privacy').prop('checked')) ? 1 : 0;
        var checkbox = ($('#checkbox').prop('checked')) ? 1 : 0;

        if(name.length < 2){
            $('#contact_form .formError').html('Geef eerst je naam.').show();
            $('#contact_form .contact__btn').show();
            return false;
        }
        if(!validateEmail(email)){
            $('#contact_form .formError').html('Geef eerst je email zodat we je kunnen bereiken.').show();
            $('#contact_form .contact__btn').show();
            return false;
        }
        if(question.length < 5){
            $('#contact_form .formError').html('Zonder bericht kunnen we je niet helpen!').show();
            $('#contact_form .contact__btn').show();
            return false;
        }
        if(!$('#checkbox').prop('checked')){
            $('#contact_form .formError').html('Ga eerst akkoord met de privacy voorwaarden.').show();
            $('#contact_form .contact__btn').show();
            return false;
        }

        $('#contact_form .formSuccess').html('Je bericht wordt verwerkt.').show();

        $.ajax({
            method: 'POST',
            url:    '/src/send_question',
            dataType:'json',
            data:   {
                name: name,
                email: email,
                tel: tel,
                question: question,
                privacy: privacy,
                checkbox: checkbox
            }
        })
        .done(function(resp){
            $('#contact_form .formSuccess').hide();
            if(resp.success){
                $('#contact_form').html('<div class="formSuccess">Je bericht is verwerkt, we nemen zo snel mogelijk contact met je op</div>');
                $('#contact_form .formSuccess').show();
            }else{
                $('#contact_form .formError').html('Kon je bericht op dit moment niet verwerken. herlaad de pagina en probeer het opnieuw.').show();
                // $('#contact_form .contact__btn').show();
            }
        })
        .fail(function(){
            $('#contact_form .formSuccess').hide();
            $('#contact_form .formError').html('Kon je bericht op dit moment niet verwerken. herlaad de pagina en probeer het opnieuw.').show();
        })
    }

    $('#contact_form').on('submit',function(event){
        sendQuestion();
        event.preventDefault();
    })

    setTimeout(function(){
        schakelCarousel({
            selector: '.logos',
            visible_items: 5
        });
    }, 300)

    /**
     * Validate an email, checks for:
     * No dot (.) as the first or last character of the email could indicate bad copy paste or missing top-level domain
     * No double dots, most likely a typo
     * No missing @
     * No . after the @
     * Missing chars before @
     * No illigal chars in email
     * @param {String} mail 
     */
    function validateEmail(mail) 
    {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,9})+$/.test(mail))
    {
        return mail;
    }
        return false;
    }
})