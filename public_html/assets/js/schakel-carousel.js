function schakelCarousel(carousel) {
    // Get the carousel container
    carousel.container = $(carousel.selector);
    // Store the with of said container for future calculations
    carousel.window_width = $(carousel.container).width();
    // Add schakel-container class to the main container
    carousel.container.addClass('schakel-carousel');
    // Collect all item and add them to a newly created schakel-carousel window
    // This is the container that we will be applying our transform on
    var item_html = carousel.container.html();
    carousel.container.html('<div class="schakel-carousel__window">' + item_html + '</div>');
    // Count the amount of items we have
    carousel.items = $('.item', carousel.container);
    carousel.item_num = carousel.items.length;
    // Set the max width per item based on the container with diveded by the max visible_items setting.
    carousel.item_width = carousel.window_width / carousel.visible_items;
    // Apply item with to the carousel items
    $('.schakel-carousel__window .item').css({
        'flex': "0 0 " + carousel.item_width + "px"
    });
    // Initial carousel position
    carousel.position = 1;
    // Handles resetting & setting the couresel timer
    var startCarouselTimeout = function (carousel) {
        // Cancel the automatic timer
        clearTimeout(carousel.interval);
        carousel.interval = setTimeout(function () {
            carouselNext(carousel);
        }, 3000);
    }
    // Check if there is a next item available. If so display if not move back to step 1
    var carouselNext = function (carousel) {
        carousel.position++;
        // Make sure we do not show any empty spaces, reset to position 1 if we navigate past the limit
        if (carousel.position > (carousel.item_num - carousel.visible_items)) {
            carousel.position = 1;
        }
        displayCarouselItem(carousel);
    }
    // Move the carousel to its new position
    var displayCarouselItem = function (carousel) {
        startCarouselTimeout(carousel);
        $('.schakel-carousel__window').css({
            "transform": "translate3d(-" + ((carousel.position - 1) * carousel.item_width) + "px, 00px, 00px)"
        });
    }
    // Only start the carousel if we have more item than we can currently display
    if (carousel.item_num > carousel.visible_items) {
        // Initiate carousel
        $('.schakel-carousel__window').css({
            "transform": "translate3d(0px, 00px, 00px)"
        });
        displayCarouselItem(carousel);
    } else {
        // If not we center the available item
        $('.schakel-carousel__window').css({
            "justify-content": "center",
            "align-items": "center"
        });
    }
}

