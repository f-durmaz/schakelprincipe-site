<?php
if (PHP_SAPI === 'cli-server' && $_SERVER['SCRIPT_FILENAME'] !== __FILE__) { return false; }
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
ini_set('display_errors','On');

require dirname(__DIR__,1) . '/vendor/autoload.php';

use Dflydev\FigCookies\FigRequestCookies;
use Dflydev\FigCookies\FigResponseCookies;
use Dflydev\FigCookies\SetCookie;

use Slim\Http\Request;
use Slim\Http\Response;

use SchakelPrincipe\EmailController;

# Get the app settings
# ----------------------------
$settings = require dirname(__DIR__,1).'/settings.php';

$app = new \Slim\App($settings);

# Set up dependencies
# ----------------------------
require dirname(__DIR__,1).'/src/dependencies.php';

# Register middleware
# ----------------------------
require dirname(__DIR__,1).'/src/middleware.php';

// Page requests
$app->get('/', function (Request $request, Response $response) {
    $response = $this->view->render($response, 'home.phtml',[]);
    return $response;
});
$app->get('/home', function (Request $request, Response $response) {
    $response = $this->view->render($response, 'home.phtml',[]);
    return $response;
});
$app->get('/privacy', function (Request $request, Response $response) {
    $response = $this->view->render($response, 'privacy.phtml',[]);
    return $response;
});

$app->get('/test', function (Request $request, Response $response) {
    $response = $this->view->render($response, 'test.phtml',[]);
    return $response;
});

// Ajax requests
$app->group('/src', function() use($app) {
    $app->post('/send_question', function(Request $request, Response $response){
        $parsedBody = $request->getParsedBody();
    
        # Honeypot to prevent bot messages
        if($parsedBody['privacy'] == 1){
            $response = $response->withJson(['success' => true]);
            return $response;
        }
    
        $EmailController = new EmailController($this);
    
        $result['data'] = $parsedBody;
        $result['success'] = $EmailController->send_question($parsedBody);
    
        $response = $response->withJson($result);
        $response = $response->withStatus(200);
    
        return $response;
    });
});


$app->run();