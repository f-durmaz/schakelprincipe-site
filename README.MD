# SchakelPrincipe

SchakelPrincipe marketing website

### Versie 1


## Installatie
Onderstaande is vereist om de php libraries te installeren & de custom classes te autoloaden. Systeem werkt niet zonder deze bestanden.
```bash
composer install
```

Om het systeem lokaal of op een eigen server te draaien is een env.php bestand in de root nodig met onderstaande data:
```php
$env =              "local";
$mailgun_key =      ''; // Mailgun key
$db_user = '';      // Kan leeg
$db_password = '';      // Kan leeg
```

De front lokaal testen kan door bovenstaande uit te voeren en een lokale php server te starten in de /public_html folder of door xampp te gebruiken
```bash
    cd public_html
    php -S localhost:8000